#!/bin/bash

#Global values
NEWLINE=""
number=1


main (){
  echo -n "Do you want to replace spaces in filenames with underscores? y/n "
  read confirm
  
  if [[ $confirm = "y" ]]||[[ $confirm = "Y" ]]||[[ $confirm = "yes" ]]||[[ $confirm = "Yes" ]]; then
    echo "Renaming..."
    replacespace
  else
    echo -n "This script will ignore all images with spaces in their addresses, do you want to continue? y/n "
    read confirm
    
    if [[ $confirm = "y" ]]||[[ $confirm = "Y" ]]||[[ $confirm = "yes" ]]||[[ $confirm = "Yes" ]]; then
      echo "continuing..."
    else
      exit 1;	
    fi
  fi
  
  makedirectory
  imagesearch
  
  echo $NEWLINE
  echo "Done."
  echo $NEWLINE
  return
}

makedirectory (){
  if [[ ! -d "./CONVERTED" ]]; then
    mkdir ./CONVERTED/
  fi
  return
}

replacespace(){
  find ./ -depth -name "* *" -execdir rename 's/ /_/g' "{}" \;
}

imagesearch (){
  echo -n "What width do you want to resize to (aspect ratio is kept)? "
  read width
  echo -n "What number do you want the filenames to start at? "
  read number
  
  for i in $( find ./ \( -type f -name "*.jpg" \) -or \( -type f -name "*.png" \) -or \( -type f -name "*.JPG" \) -or \( -type f -name "*.PNG" \) ); do
    owidth=$(identify -format "%w" "$i");
    b=${i: -4};
    if [ $owidth -gt $width ]; then
      convert -resize $width "$i" ./CONVERTED/$number$b;
    else
      cp $i ./CONVERTED/$number$b;
    fi
    number=$(($number+1))
  done
  return
}
main
