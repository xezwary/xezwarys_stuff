#!/bin/bash

#Global values
Some_Variable=false
Images_Array=()

main(){
	imagesearch
	
	while [[ $Some_Variable = false ]]; do
		image=${Images_Array[$RANDOM % ${#Images_Array[@]} ]} &&
		
		gsettings set org.gnome.desktop.background picture-uri file://$PWD/$image &&
		
		echo "" &&
		echo $PWD'/'$image &&
		echo 'Wallpaper changed, sleeping...' &&
		sleep 300
	done
}

imagesearch (){
	for i in $( find ./ \( -type f -name "*.jpg" \) -or \( -type f -name "*.png" \) -or \( -type f -name "*.JPG" \) -or \( -type f -name "*.PNG" \) ); do
		b=${i: -4} &&
		c=${i: 0:2} &&

		if [[ $b = ".jpg" ]] || [[ $b = ".JPG" ]] || [[ $b = ".png" ]] || [[ $b = ".PNG" ]] && [[ $c = "./" ]] && [[ ! "$i" = *"nsfw"*  ]] && [[ ! "$i" = *"NSFW"*  ]]; then
			Images_Array+=(${i: 2})
		else
			i=false
		fi
	done
	return
}

main
