#!/bin/bash

Images_Array=()
main(){
	imagesearch
	
	while true; do
		image=${Images_Array[$RANDOM % ${#Images_Array[@]} ]} &&
    feh --bg-fill "$image" &&
		
		echo "" &&
		echo "$PWD/$image" &&
		echo 'Wallpaper changed, sleeping...' &&
		sleep 300
	done
}

imagesearch (){
	for i in $( find ./ \( -type f -name "*.jpg" \) -or \( -type f -name "*.png" \) -or \( -type f -name "*.JPG" \) -or \( -type f -name "*.PNG" \) ); do
		b=${i: -4} &&
		c=${i: 0:2} &&

		if [[ $b = ".jpg" ]] || [[ $b = ".JPG" ]] || [[ $b = ".png" ]] || [[ $b = ".PNG" ]] && [[ $c = "./" ]]; then # && [[ ! "$i" = *"nsfw"*  ]] && [[ ! "$i" = *"NSFW"*  ]]; then
			Images_Array+=(${i: 2})
		else
			i=false
		fi
	done
	return
}

main
