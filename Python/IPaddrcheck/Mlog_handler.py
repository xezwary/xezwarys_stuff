# -- coding: utf-8 --

import logging
from sys import path
from shlex import split
from os.path import join
from time import asctime, localtime, time
from subprocess import check_output, CalledProcessError

# Sends notification to desktop with bash's notify-send, 
# and writes errors to the log.
def log_handler(a, b):
  logging.basicConfig(
    filename = str(join('{0}', 'IPaddrcheck.log')).format(path[0]), 
    level = logging.INFO
  )
  notification = [
    str(
      'notify-send -u normal ' + 
      '-i {0}/icon.png \'IPaddrcheck\' {1}'.format(path[0], '{0}')
    ), 
    'Could not send notification to desktop...\n{0}', 
    'Could not send notification!\nCheck the log for the deets!'
  ]
  
  if a == 'pingnotreceived':
    try:
      check_output(split(str(
        notification[0].format('\'Ping not received from: {0}\''.format(b))
      )))
    except CalledProcessError as e:
      logging.warning(notification[1].format(e))
      print(notification[2])
    
    logging.info(str(
      '\n{0}\nPing not received from: {1}'.format(
        asctime(localtime(time())), 
        b
      )
    ))
  
  elif a == 'pingfailed':
    try:
      check_output(split(str(
        notification[0].format('\'Could not ping {0}\''.format(b))
      )))
    except CalledProcessError as e:
      logging.warning(notification[1].format(e))
      print(notification[2])
    
    logging.info(str(
      '\n{0}\nCould not ping: {1}'.format(
        asctime(localtime(time())), 
        b
      )
    ))
  
  elif a == 'unknown':
    try:
      check_output(split(str(
        notification[0].format(
          '\'Unknown error occurred, check the log for the deets!\''
        )
      )))
    except CalledProcessError as e:
      logging.warning(notification[1].format(e))
      print(notification[2])
    
    logging.warning(str(
      '\n{0}\nUnknown error occurred:\n{1}\n'.format(
        asctime(localtime(time())), 
        b
      )
    ))
