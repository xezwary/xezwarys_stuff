# -- coding: utf-8 --
'''
Python 2.7-3.5
Checks IP addresses, logs IP addresses that don't respond.
Multithreaded!
'''

import threading
from time import sleep
from shlex import split
from os.path import join
from sys import exit, path
from psutil import cpu_count
from clint.textui import colored
from signal import signal, SIGINT
from Mlog_handler import log_handler
from subprocess import check_output, CalledProcessError


# Handles ctrl+c event.
def handler(a, b):
  if a == 2:                                                      # SIGINT
    for i in Looper.threads:                                      # Will only work locally in main...
      i.join()                                                    # Waits for threads to finish, so that you'll get a gracefull exit.
    exit(0)


# Pings IP addresses with ping.
def ping(adds):
  ping = 'ping -W 3 -q -c 3 {0}'.format(adds)
  
  try:
    check_output(split(ping))
    print('{0} Ping received from {1}'.format(
      colored.green('[+]'), adds
    ))
  
  except CalledProcessError as e:
    returncode = int(str(e)[-1])
    
    if returncode == 1:
      print('{0} Ping not received from {1}'.format(
        colored.red('[-]'), adds
      ))
      log_handler('pingnotreceived', adds)
    
    elif returncode == 2:
      print('{0} Could not ping {1}'.format(
        colored.red('[-]'), adds
      ))
      log_handler('pingfailed', adds)
  
  except Exception as e:                                          # Catches unexpected exceptions.
    print(
      '{0} Unknown error, check the log for the deets!'.format(
        colored.red('[-]')
      )
    )
    log_handler('unknown', e)
  
  # Nmap alternative code below...
  #----------------------------------------------                 # Legacy code
  '''
  With popen, you might as well not use multithreading...
  Trashed!
  
  nmap = 'nmap -sn -v -oG - '+adds
  grep = 'grep Status'
  
  com1 = Popen(split(nmap), stdout=PIPE,)
  com2 = Popen(split(grep), stdin=com1.stdout, stdout=PIPE,)
  
  end = com2.stdout
  for b in end:
    endstring = str(b.strip())
    print endstring
    if endstring[-2:] != 'Up':
      handler('pingnotreceived', adds)
  '''
  #----------------------------------------------                 # End of legacy code


# This is the life of the threads.
# they do their jobs, then grab more, until there's no more.
def thread_run():
  while len(Looper.jobs) != 0:
    local_job = str(Looper.jobs[-1])
    del Looper.jobs[-1]                                           # Deletes the job so the other threads don't repeat it.
    
    ping(local_job)


# The thread creator, also some global vars.
class Looper:
  threads = []
  jobs = []                                                       # Will contain IP addresses to be checked, will be removed as threads go through it.
  
  @classmethod
  def run(cls, adds):
    for i in adds:
      cls.jobs.append(i)
    
    for i in range(cpu_count()):                                  # Will create as many threads as there are cores.
      if len(cls.jobs) != 0:                                      # Unless there aren't enough jobs.
        t = threading.Thread(
          target = thread_run, 
        )
        
        t.start()
        cls.threads.append(t)
      else:
        continue
    
    for i in cls.threads:
      i.join()
    
    del cls.threads[:]                                            # Deletes the threads from the list, instead of just making a new list.


def main():
  signal(SIGINT, handler)                                         # Will handle ctrl+c event.
  addresses = []                                                  # Addresses
  
  with open(str(join('{0}', 'addresses.txt')).format(path[0]), 'r') as i:       # Grabs IP addresses from text file.
    for b in i.readlines():
      addresses.append(b)
  
  print('\nTo stop, press ctrl+c\n')
  while True:                                                     # Will loop forever and ever and ever and ever and eve-
    print('Pinging...\n')
    Looper.run(addresses)
    
    print('\nsleeping...')
    sleep(600)


if __name__ == '__main__':
  main()
