# -- coding: utf-8 --
"""
Python 3
From list of paths, create multiline tree-like string.
"""

from sys import platform
from os.path import join, basename

def read(paths):
  return ReadablePath.read(paths)

class ReadablePath:
  paths = []
  
  @classmethod
  def read(cls, paths):
    for i in paths:
      cls.paths.append(i)                                                                 # Append all strings from given list to ReadablePath.paths.
    
    paths.sort()
    cls.paths.sort()
    result = ''
    
    separator = '/'
    root = '/'
    if platform.startswith('win') or platform == 'cygwin':                                # Check if Windows.
      separator = '\\'
    
    for i in paths:
      if separator == '\\':
        root = str(i[:2]+'\\')                                                            # Get the Disk letter as root.
        result += cls.stringScan(i[2:], separator, root)
        continue
      result += cls.stringScan(i, separator, root)
    
    return str(result[:-2])
  
  @classmethod
  def stringScan(cls, path, separator, root):
    newpath = root
    files = []
    lpath = path.split(separator, 1)
    while len(lpath) > 1:                                                                 # Build the full path
      newpath = str(join(newpath, lpath[0]))
      lpath = lpath[1].split(separator, 1)
    
    for i in cls.paths:
      if str(i[:-len(basename(i))]) == join(newpath, ''):                                 # Find the files within the directory.
        files.append(basename(i))
    for i in files:
      cls.paths.remove(join(newpath, i))                                                  # Remove scanned paths.
    
    if len(files) != 0:
      newpath = join(newpath, '')
      newpathf = newpath                                                                  # We'll need the length of the full path.
      for i in files:
        newpathf += str(str('\n'+' '*len(newpath))[:-1]+'^'+i)                            # Build the multiline string, with tabbed filenames,
      return newpathf + '\n\n'                                                            # similar to a tree structure, but not really.
    return ''                                                                             # Return empty string if no files were found.
