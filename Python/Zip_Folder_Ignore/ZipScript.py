# -- coding: utf-8 --
"""
Python 3
Extracts archives in really specific ways.
"""
from zipfile import ZipFile, is_zipfile, ZipInfo as zinf
#from rarfile import RarFile as ZipFile, is_rarfile as is_zipfile, RarInfo as zinf
from os import makedirs
from os.path import join, basename, exists, isfile, isdir
from clint.textui import colored

"""
-- How to use --

ZipScript.extract(STRING, TUPLE, DICTIONARY)

Options:
1. TUPLE: One or more archives to extract.
2. STRING: Path the script will extract to.
3. DICTIONARY:
  1. 'overwrite': Boolean, will not overwrite if True, renames files.
  2. 'indfolder': Boolean, each zip will be extracted to its own folder.
  3. 'foldertofile': Boolean, gives file the folder as name.

Extracts only files from archives, check python zipfile docs for compatibility.

-- How to use --
"""
def extract(filename, dirname, **kwargs):
  obj = ZipScript(**kwargs)
  obj.extract(filename, dirname)


class ZipScript:
  safechar = (' ','.','_')
  @classmethod
  def __init__(cls, **kwargs):
    cls.kwargs = kwargs
  @classmethod
  def extract(cls, filename, dirname):
    for i in filename:
      if is_zipfile(i) != True:
        print('{0} {1}\n'.format(colored.red('Invalid file:'), i))
        continue
      
      if cls.kwargs['indfolder'] == True:                                                 # Sets dirname so that the zip is extracted to its own folder.
        original_dirname = dirname
        dirname = join(dirname, cls.findName(i))
      if exists(dirname) != True:
        makedirs(dirname)
      
      cls.writeFile(i, dirname)
      
      if cls.kwargs['indfolder'] == True:
        dirname = original_dirname                                                        # Return path to original path, so as to not mess up folder-structure.
  
  @classmethod
  def writeFile(cls, filename, dirname):
    with ZipFile(filename, 'r') as myzip:                                                 # Open archive.
      #myzip.setpassword(b'')
      for File in myzip.namelist():
        if len(str(basename(File))) < 1 or zinf.is_dir(myzip.getinfo(File)) == True:      # If basedir doesn't return anything, it's just a folder.
          continue
        
        File_Write = str(join(dirname, cls.safePath(basename(File))))
        if cls.kwargs['foldertofilename'] == True:                                        # Gives file the folder as name.
          File_Write = cls.folderAppend(File_Write, File)
        if cls.kwargs['overwrite'] == True or isdir(File_Write):                          # Check if file exists, if it does, append incrementing number.
          File_Write = cls.nameAppend(File_Write)
        
        with open(File_Write, 'wb') as files:                                             # Creates/truncates file, then writes binary.
          files.write(myzip.read(File))
  
  # Find filename, example: /home/user/zip_file.zip > zip_file
  @staticmethod
  def findName(path):
    ext_len = len(path)
    for b in range(1, len(path)):                                                         # Find position of first slash and extension.
      if path[-b] == '/' or path[-b] == '\\':
        name_len = b
        break
    for b in range(0, len(path)):
      if path[-b] == '.':
        ext_len = b
        break
    return path[-name_len+1:-ext_len]
  
  # Gives file the folder as name, example: haakon/file.zip > haakon.zip
  @classmethod
  def folderAppend(cls, path, fldr):
    fldr = str(join(' ', fldr))
    fldb = 0
    flde = -1
    ext_len = -1
    for i in range(0, len(fldr)):                                                         # Find slashes in path, to find out the foldername.
      if flde == -1 and fldr[-i] == '/' or flde == -1 and fldr[-i] == '\\':
        flde = -i
        continue
      if fldr[-i] == '/' or fldr[-i] == '\\':
        fldb = -i
        break
    for i in range(1, len(path)):
      if path[-i] == '/' or path[-i] == '\\':
        name_len = -i
        break
    for i in range(0, len(path)):                                                         # Find extension position.
      if path[-i] == '.':
        ext_len = -i
        break
      elif -i == flde:
        break
    if fldr[fldb:flde] == ' ':
      print('\n{0} \n{1}\n'.format(
        colored.red('No folder to use as filename, using:'), path
      ))
      return path
    if ext_len == -1:                                                                     # If no extension found.
      return str(
        join(path[:name_len], cls.safePath(fldr[fldb:flde])+path[ext_len:])
      )[:-1]
    else:                                                                                 # If extension found.
      return str(
        join(path[:name_len], cls.safePath(fldr[fldb:flde])+path[ext_len:])
      )
  
  # Check if file exists, if it does, append incrementing number.
  @staticmethod
  def nameAppend(path):
    ext_len = None
    newpath = path
    num = 1
    for i in range(len(basename(path))):                                                  # Find position of extension and slash.
      if path[-i] == '.':
        ext_len = i
        break
    while isfile(newpath) or isdir(newpath) == True:                                      # To keep incrementing while file or folder exists.
      newpath = path
      if ext_len != None:
        newpath = str('{0}{1}{2}').format(
          newpath[:-ext_len], 
          num, 
          newpath[-ext_len:len(newpath)]
        )
      else:
        newpath = str('{0}{1}').format(newpath, num)
      num += 1
    return newpath
  
  # Checks for and replaces a couple invalid characters.
  @classmethod
  def safePath(cls, path):
    return ''.join(
      c for c in path if c.isalnum() or c in cls.safechar
    ).rstrip()
