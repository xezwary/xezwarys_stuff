# -- coding: utf-8 --
"""
Python 3
Extract Files from zip, moves them to root of directory, then deletes 
folder structure.
"""

from pickle import load, dump
from ReadablePath import read
from ZipScript import extract
from sys import path, platform
from tkinter import *
from tkinter.ttk import *
from clint.textui import colored
from os import remove, environ, mkdir
from os.path import join, isfile, isdir, normpath
from tkinter.messagebox import showinfo, OK
from tkinter.filedialog import askopenfilename, askdirectory


class Zipex():
  # Gotta store these values somewhere I can get them...
  indfolder = 0 
  overwrite = 0 
  foldertofile = 0
  
  def __init__(self):
    # Directory to scan/extract to.
    self.dirname = None
    
    # --- Top labels ---                                                                  # --- Top labels ---
    Label(mainframe, text='Extract to:').grid(
      row=2, column=0, sticky='W', pady=5, padx=5
    )
    self.l2text = StringVar()
    self.l2text.set('No folder selected...')
    l2 = Label(
      mainframe, textvariable=self.l2text
    ).grid(
      row=2, column=2, columnspan=4, sticky='WE', pady=5, padx=5
    )
    # --- Top labels ---                                                                  # --- Top labels ---
    
    
    # --- Buttons ---                                                                     # --- Buttons ---
    # Get directory
    Button(
      mainframe, text='Browse', 
      command=self.askdirectory
    ).grid(row=2, column=1, sticky='W', pady=5, padx=5)
    
    # Manage items in listbox
    Button(
      mainframe, text='Add files', command=self.askopenfilename
    ).grid(row=5, column=0, sticky='NW', pady=5, padx=5)
    Button(
      mainframe, text='Remove', command=self.remove
    ).grid(row=5, column=1, sticky='NW', pady=5, padx=5)
    Button(
      mainframe, text='Clear', command=self.clear
    ).grid(row=5, column=2, sticky='NW', pady=5, padx=5)
    
    # Starts script
    self.start = Button(
      mainframe, text='Start', command=self.start, state=DISABLED
    )
    self.start.grid(row=5, column=3, sticky='W', pady=5, padx=25)
    # --- Buttons ---                                                                     # --- Buttons ---
    
    
    # --- Scrollbar ---                                                                   # --- Scrollbar ---
    self.scrollbarv = Scrollbar(mainframe)
    self.scrollbarv.grid(row=6, column=5, sticky='NWS')
    # --- Scrollbar ---                                                                   # --- Scrollbar ---
    
    
    # --- Listbox ---                                                                     # --- Listbox ---
    self.listbox = Listbox(
      mainframe, selectmode=EXTENDED, height=10, 
      yscrollcommand=self.scrollbarv.set
    )
    self.listbox.grid(row=6, column=0, columnspan=5, sticky='NWES')
    self.scrollbarv.config(command=self.listbox.yview)
    # --- Listbox ---                                                                     # --- Listbox ---
    
    
    # --- Options ---                                                                     # --- Options ---
    self.indfolder = IntVar()
    self.overwrite = IntVar()
    self.foldertofile = IntVar()
    self.indfolder.set(Zipex.indfolder)
    self.overwrite.set(Zipex.overwrite)
    self.foldertofile.set(Zipex.foldertofile)
    
    Label(mainframe, text='Options:').grid(
      row=1, column=0, sticky='W', pady=5, padx=5
    )
    self.individualdirbool = Checkbutton(
      mainframe, 
      text='New folder for each zip', 
      variable=self.indfolder, 
      command=self.config
    )
    self.individualdirbool.grid(
      row=1, column=1, columnspan=2, sticky='W', pady=5, padx=5
    )
    
    self.overwritebool = Checkbutton(
      mainframe, 
      text='Don\'t overwrite', 
      variable=self.overwrite, 
      command=self.config
    )
    self.overwritebool.grid(
      row=1, column=3, columnspan=2, sticky='W', padx=5, pady=5
    )
    
    self.foldertofilebool = Checkbutton(
      mainframe, 
      text='Use folder as filename', 
      variable=self.foldertofile, 
      command=self.config
    )
    self.foldertofilebool.grid(
      row=1, column=4, sticky='W', padx=5, pady=5
    )
    # --- Options ---                                                                     # --- Options ---
    
    
    # --- Options for askopenfilename ---                                                 # --- Options for askopenfilename ---
    self.file_opt = options = {}
    options['defaultextension'] = '.zip'
    options['filetypes'] = [('zip files', '.zip'), ('all files', '.*')]
    options['initialdir'] = '%userprofile%'
    options['parent'] = mainframe
    options['title'] = ''
    options['multiple'] = 1
    # --- Options for askopenfilename ---                                                 # --- Options for askopenfilename ---
    
    
    # --- Options for askdirectory ---                                                    # --- Options for askdirectory ---
    self.dir_opt = options = {}
    options['initialdir'] = '%userprofile%'
    options['mustexist'] = True
    options['parent'] = mainframe
    options['title'] = ''
    # --- Options for askdirectory ---                                                    # --- Options for askdirectory ---
  
  
  # Remove item from listbox
  def remove(self):
    for i in reversed(self.listbox.curselection()):
      self.listbox.delete(i)
    self.state_check()
  
  # Remove all items from listbox
  def clear(self):
    self.listbox.delete(0, END)
    self.state_check()
  
  # Get filenames
  def askopenfilename(self):
    files = list(askopenfilename(**self.file_opt))
    for i in reversed(files):
      if i in self.listbox.get(0, END):
        files.remove(i)
    if len(files) < 1:
      pass
    elif len(files[0]) < 2:
      pass
    else:
      for i in files:
        self.listbox.insert(END, str(normpath(i)))
    
    self.state_check()
  
  
  # Get directory
  def askdirectory(self):
    self.dirname = None
    directory = join(
      join(
        str(askdirectory(**self.dir_opt)), 
        'EXTRACTED'
      ), ''
    )
    if len(directory) < 1:                                                                # Check if selection is valid.
      self.l2text.set('No folder selected...')
    elif directory == join(join('()', 'EXTRACTED'), ''):
      self.l2text.set('No folder selected...')
    elif directory == join('EXTRACTED', ''):
      self.l2text.set('No folder selected...')
    elif directory == '.\\EXTRACTED\\' or directory == './EXTRACTED/':
      self.l2text.set('No folder selected...')
    else:
      self.dirname = directory
      self.l2text.set(str(self.dirname))
    self.state_check()
  
  # Check if requirements are met, directory and archive file...
  def state_check(self):
    print('\n\n{0}\n\n{1}'.format(
      read(list(self.listbox.get(0, END))), colored.yellow(self.dirname))
    )
    
    for i in range(0, len(self.listbox.get(0, END)), 2):                                  # Add some color to every other line in the listbox
      self.listbox.itemconfigure(i, background='#f0f0ff')
      if len(self.listbox.get(0, END))-1 > i:
        self.listbox.itemconfigure(
          i+1, background=self.listbox.cget('background')
        )
    
    if self.dirname == None or len(self.listbox.get(0, END)) < 1:
      self.start['state'] = 'disabled'
    elif self.dirname == join(join('()', 'EXTRACTED'), ''):
      self.start['state'] = 'disabled'
    elif self.dirname == join('EXTRACTED', ''):
      self.start['state'] = 'disabled'
    else:
      self.start['state'] = 'normal'
  
  # Start script
  # Creates a child window, and runs script.
  def start(self):
    self.start['state'] = 'disabled'
    self.child = Toplevel()
    self.child.title('Extracting...')
    self.chmf = Frame(self.child)
    self.chmf.grid(column=0, row=0, sticky='NWES')
    
    self.child.columnconfigure(0, weight=1)
    self.child.rowconfigure(0, weight=1)
    self.chmf.columnconfigure(0, weight=1)
    self.chmf.rowconfigure(2, weight=1)
    
    self.child.geometry('0x0')
    self.child.minsize(250, 90)
    self.childWindow()
  
  # The child window widgets
  def childWindow(self):
    self.top_text = StringVar()
    self.prgs = IntVar()
    self.progrs = int(0)
    self.count = len(self.listbox.get(0, END))
    
    Label(self.chmf, textvariable=self.top_text).grid(
      row=0, column=0, pady= 5, padx=5, sticky='N'
    )
    
    # --- Progressbar ---                                                                 # --- Progressbar ---
    self.prgs.set(self.progrs)
    self.progress = Progressbar(
      self.chmf, maximum=self.count, variable=self.prgs, mode='determinate', 
      orient=HORIZONTAL
    )
    self.progress.grid(row=1, column=0, padx=10, sticky='WE')
    # --- Progressbar ---                                                                 # --- Progressbar ---
    
    # --- Buttons ---                                                                     # --- Buttons ---
    self.next = Button(
      self.chmf, text='Continue', command=self.childQuit, state=DISABLED
    )
    self.next.grid(row=2, column=0, sticky='SE', pady=5, padx=5)
    # --- Buttons ---                                                                     # --- Buttons ---
    
    
    # Runs script
    bools = {
      'overwrite': self.overwritebool.instate(['selected']), 
      'indfolder': self.individualdirbool.instate(['selected']), 
      'foldertofilename': self.foldertofilebool.instate(['selected'])
    }
    for i in self.listbox.get(0, END):
      filename = [i]
      extract(filename, str(self.dirname), **bools)
      
      self.progrs += 1
      self.prgs.set(self.progrs)
      self.top_text.set(str('Extracting {0}/{1}').format(
        self.progrs, self.count
      ))
      
    self.next['state'] = 'normal'                                                         # You are now allowed to continue, as the script has finished.
    self.top_text.set(str('Extracting {0}/{1}, completed.').format(
      self.progrs, self.count
    ))
    self.prgs.set(self.count)
    
  # Enables the start button on root window, 
  # and destroys the child, deleting all its widgets...
  def childQuit(self):
    self.start['state'] = 'normal'
    self.child.destroy()
  
  # Update the values.
  def config(self):
    Zipex.indfolder = int(self.indfolder.get())
    Zipex.overwrite = int(self.overwrite.get())
    Zipex.foldertofile = int(self.foldertofile.get())


def about():
  showinfo(
    'About', 'This program was created by:\nHåkon J. Pedersen', default=OK
  )

def quit():
  root.quit()


if __name__=='__main__':
  root = Tk()
  root.title('Zip Folder Ignore')
  mainframe = Frame(root)
  mainframe.grid(column=0, row=0, sticky='NWES')
  
  root.columnconfigure(0, weight=1)
  root.rowconfigure(0, weight=1)
  mainframe.columnconfigure(4, weight=1)
  mainframe.rowconfigure(6, weight=1)
  
  root.geometry('0x0')
  root.minsize(650, 345)
  
  remember = BooleanVar()
  rememberval = 0
  save_loc = join(path[0], 'zip_folder_ignore.dat')
  if platform.startswith('win') or platform == 'cygwin':
    platform = 'win'
    save_loc = join(join(environ['APPDATA'], 'Zip_Folder_Ignore'),
      'zip_folder_ignore.dat'
    )
  if isfile(save_loc):
    with open(save_loc, 'rb') as f:                                                       # Load options from saved file if it exists.
      Zipex.indfolder, Zipex.overwrite, Zipex.foldertofile, rememberval = load(f)
  remember.set(rememberval)
  
  # --- Menubar ---                                                                       # --- Menubar ---
  menubar = Menu()
  filemenu = Menu(menubar, tearoff=0)
  toolmenu = Menu(menubar, tearoff=0)
  
  menubar.add_cascade(label='File', menu=filemenu)
  filemenu.add_command(label='About', command=about)
  filemenu.add_command(label='Quit', command=quit)
  menubar.add_cascade(label='Tools', menu=toolmenu)
  toolmenu.add_checkbutton(
    label='Remember options', 
    onvalue=1, 
    offvalue=0, 
    variable=remember
  )
  root.config(menu=menubar)
  # --- Menubar ---                                                                       # --- Menubar ---
  
  Zipex()
  root.mainloop()
  
  if remember.get() == True:                                                              # Either save or delete saved options
    rememberval = remember.get()
    if(
      isdir(save_loc[:-len('zip_folder_ignore.dat')]) != True and
      platform == 'win'
    ):
      mkdir(save_loc[:-len('zip_folder_ignore.dat')])
    with open(save_loc, 'wb') as f:
      dump([
        Zipex.indfolder, 
        Zipex.overwrite, 
        Zipex.foldertofile, 
        rememberval
      ], f, 2)
  else:
    if isfile(save_loc):
      remove(save_loc)
