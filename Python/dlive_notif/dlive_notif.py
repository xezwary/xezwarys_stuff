#!/usr/bin/python3
"""
Dlive notifications for desktop using webscrapers.
Geckodriver required in the cwd.
Json document will be written in cwd.

Currently only works on Linux, unless notify-send is commented out...
Xezwary
"""
from argparse import ArgumentParser, RawTextHelpFormatter
from contextlib import closing
from selenium.webdriver import Firefox
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from urllib import request
from urllib.error import URLError
from bs4 import BeautifulSoup
from os import getcwd
from os.path import join, isfile
from shlex import split
from time import sleep
from subprocess import call
from json import dump, load
from clint.textui import colored
from tqdm import tqdm
from signal import signal, SIGINT

def handler(signum, b):
  if signum == 2:
    exit(0)

def checkstreams(following):
  livechannels = {}
  for i in following:
    url = 'https://dlive.tv{0}'.format(i)
    soup = BeautifulSoup(request.urlopen(url).read(), 'lxml')
    item = soup.find('img', {'src' : '/img/live.4729dfd6.svg'})
    if item:
      item = soup.find('img', {'src' : '/img/livestream-rerun.a36be011.svg'})
      if item:
        print('{0} is {1}!'.format(i[1:], colored.yellow('rerunning')))
        livechannels[i[1:]] = False
        pass
      else:
        print('{0} is {1}!'.format(i[1:], colored.green('live')))
        livechannels[i[1:]] = True
    else:
      print('{0} is {1}.'.format(i[1:], colored.red('offline')))
      livechannels[i[1:]] = False
  return(livechannels)

def main(following):
  print(following, len(following))
  if len(following) == 0:
    print('No following channels found...')
    exit(0)
  notified = {}
  for i in following:
    notified[i[1:]] = False
  
  while True:
    print('\nChecking streams...')
    statuses = checkstreams(following)
    for i in statuses:
      if statuses[i] == False:
        notified[i] = False
        continue
      if notified[i] != True:
        call(split(
          'notify-send -u normal \'{0} is live!\' \'dlive_notif\''.format(i)
        ))
        notified[i] = True
    
    print('Sleeping for 10 minutes...')
    for i in tqdm(range(600)):
      sleep(1)

if __name__ == '__main__':
  parser = ArgumentParser(
    description='Parse xml.',
    formatter_class = RawTextHelpFormatter
  )
  parser.add_argument('-u', '--user', type = str, default = None,
            help = 'Dlive Username')
  parser.add_argument('-r', '--refresh',
            action='store_true', default = False,
            help = 'Refresh following list')
  args = parser.parse_args()
  if(
    args.refresh and not args.user or
    not isfile(join(getcwd(), 'following.json')) and not args.user
  ):
    print('Username required to fetch following list...')
    exit(1)
  signal(SIGINT, handler)
  
  # use firefox to get page with javascript generated content
  if not isfile(join(getcwd(), 'following.json')) or args.refresh == True:
    with closing(Firefox(executable_path=join(getcwd(), 'geckodriver'))) as driver:
      driver.get("https://dlive.tv/{0}".format(args.user))
      button = driver.find_elements_by_class_name('profile-tab-item')
      button[-1].click()
      # wait for the page to load
      element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "pl-4"))
      )
      
      #scr1 = driver.find_element_by_xpath("//div[contains(@class,'avatar-wrapper')]/../../..//div[contains(@class, 'flex-box')]")
      while True:
        # Get list count
        elemsCount = driver.execute_script("return document.querySelectorAll('.pl-4 > .flex-align-center > .avatar-wrapper').length")
        
        # Scroll down to bottom - If we could, Dlive sucks... We'll just have to ask the user to do it...
        #driver.execute_script("arguments[0].scrollTo(0, arguments[0].scrollHeight);", scr1)
        call(split(
          'notify-send -u normal \'Please scroll down for me... :(\' \'dlive_notif\''
        ))
        # Wait to load new elements
        try:
          WebDriverWait(driver, 10).until(
            lambda x: x.find_element_by_xpath(
              "//div[contains(@class, 'pl-4')]/a[contains(@class,'flex-align-center')]/div[contains(@class,'avatar-wrapper')][{0}]".format(str(elemsCount+1))
            )
          )
        except:
          break
      
      # store page to string variable
      page_source = driver.page_source
    
    soup = BeautifulSoup(page_source, 'lxml')
    items = soup.findAll('div', class_='v-window-item')
    following = []
    for i in items[-3]:
      links = i.findAll('a')
      for a in links:
        following.append(a['href'])
    
    print('Saving follow list to following.json...')
    with open(join(getcwd(), 'following.json'), 'w') as f:
      dump(following, f)
  else:
    print('Loading saved follow list...')
    with open(join(getcwd(), 'following.json'), 'r') as f:
      following = load(f)
  following = sorted(following)
  
  while True:
    try:
      main(following)
    except URLError:
      print('Failed to resolve name, trying again in 10 seconds...')
      sleep(10)
      pass
