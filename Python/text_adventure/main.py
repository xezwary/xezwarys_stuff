# -- coding: utf-8 --
'''
Python 3
A very simple text adventure game with rpg elements.
'''

import re
import pickle

from sys import path
from random import randint
from os.path import join, isfile
from signal import signal, SIGINT
from text_adventure_handler import handler
from text_adventure_characters import Hero, Enemy


class Game:
  def __init__(self):
    self.keep_playing = True
    self.locations = {
      'lvl1': {
        '1': 'work',
        '2': 'grocery store',
        '3': 'dungeon',
        '4': 'shop',
        '5': 'bed',
        '6': 'vacation',
        '7': 'view character',
        '8': 'suicide',
      },
      'lvl2': {
        '1': 'home',
        '2': 'huge mountain trek'
      },
    }
    
    self.weapons = {'claws': [35, 0], 'fists': [20, 0]}
    self.items = {
      'potion': {
        'message': 'Ye health hath goneth upeth, sire.',
        'hp': 100
      }
    }
    
    self.weapons_prices = {'claws': 100, 'fists': 0}
    self.items_prices = {'potion': 25}
    
    self.skills = {
      '1': {
        'claws': {'Super Claws': [45, 5]}, 
        'fists': {'Manly Fisticuffs': [35, 5]}
      },
      '3': {
        'claws': {'Mega Super Claws': [70, 10]}
      }
    }
    
    self.enemies = {
      'lvl1': {
        0: {
          'name': 'Crab',
          'hp': 20,
          'lvl': 1,
          'expgive': 25,
          'weapon': 'claws'
        },
        1: {
          'name': 'Human',
          'hp': 100,
          'lvl': 1,
          'expgive': 50,
          'weapon': 'fists'
        }
      },
      'lvl3': {
        0: {
          'name': 'Yeti-Bear Hybrid',
          'hp': 135,
          'lvl': 3,
          'expgive': 100,
          'weapon': 'claws'
        }
      },
    }
  
  
  def battle(self, enemies_list):
    enemy = enemies_list[randint(0, int(len(enemies_list)-1))]
    encounter = Enemy(**enemy)
    # -- BATTLE --
    print('\nYe encounteth a level {0} {1}, sire.'.format(
      encounter.level, 
      encounter.name
    ))
    
    defeated = False
    while defeated != True:
      enemy_attack = None                                                       # -- Enemy turn --
      while not enemy_attack:
        for level in self.skills:
          if int(level) == int(randint(1, int(encounter.level))):
            for weapon, skill_list in self.skills[level].items():
              if weapon == encounter.weapon:
                if randint(0, 100) > 35:
                  enemy_attack = weapon
                  damage = int(self.weapons[enemy_attack][0])
                else:
                  looped = 0
                  loops = randint(0, int(len(skill_list)-1))
                  for skill, values in skill_list.items():
                    if looped != loops:
                      looped += 1
                      continue
                    else:
                      enemy_attack = skill
                      damage = values[0]
                      break
              else:
                continue
          else:
            continue
      
      print('\n{0} attacketh with {1}, sire.'.format(
        encounter.name, 
        enemy_attack
      ))
      if randint(0, 1000) > 200:
        self.character.hpSet(int(self.character.hp - damage))
      else:
        print('{0} hath misseth, sire.'.format(encounter.name))
      
      if self.character.hp <= 0:
        print('\nYe hath beeneth killeth, sire.')
        defeated = True
        self.keep_playing = False
        break                                                                   # -- Enemy turn --
      
      is_true = True                                                            # -- Character turn --
      while is_true != False:
        choices = {'1': 'Attack', '2': 'Items'}
        print('')
        for i in choices:
          print('{0} {1}, sire.'.format(i, choices[i]))
        
        while is_true != False:
          choice = str(input())
          for i in choices:
            if choice == str(i):
              choice = str(choices[i])
              is_true = False
        
        if choice == 'Attack':                                                    # -- Attack --
          print('\nHow doth thee wisheth to attack thy enemy, sire?')
          looped = 1
          print('{0} {1}, sire.'.format(looped, self.character.weapon))
          
          attacks = {}
          attacks[str(looped)] = {}
          attacks[
            str(looped)][str(self.character.weapon)
          ] = self.weapons[self.character.weapon]
          
          looped += 1
          for level in self.skills:
            if int(level) <= int(self.character.level):
              for weapon, skill_list in self.skills[level].items():
                if weapon == self.character.weapon:
                  for skill, values in skill_list.items():
                    print('{0} {1}, sire.'.format(looped, skill))
                    
                    attacks[str(looped)] = {}
                    attacks[str(looped)][skill] = values
                    
                    looped += 1
                else:
                  continue
            else:
              continue
          
          print('{0} Return, sire.'.format(len(attacks)+1))
          is_true = False
          while is_true != True:
            is_true = False
            while is_true != True:
              choice = str(input())
              for i in attacks:
                if choice == str(i) or choice == str(len(attacks)+1):
                  is_true = True
            
            if int(choice) == len(attacks) + 1:
              is_true = True
              break
            else:
              for i in attacks[choice]:
                attack = i
                damage = attacks[choice][i][0]
                stamina_use = attacks[choice][i][1]
              
              if self.character.stamina < stamina_use and stamina_use > 0:
                print('Ye haveth not enough stamina, sire.')
                is_true = False
              else:
                is_true = True
          
          if int(choice) == len(attacks) + 1:
            is_true = True
          else:
            print('\nYe attacketh with {0}, sire.'.format(attack))
            if randint(0, 1000) > 200:
              encounter.hpDown(damage)
              print('Enemy hath taken {0} wounds, sire.'.format(damage))
            else:
              print('Ye hath missed, sire.')
            
            if stamina_use != 0:
              print('')
              self.character.staminaDown(stamina_use)
            is_true = False                                                       # -- Attack --
        
        elif choice == 'Items':                                                   # -- Items --
          looped = 1
          items = {}
          
          for i in self.character.inventory:
            if self.character.inventory[i] > 0:
              items[str(looped)] = {}
              items[str(looped)] = i
              
              looped += 1
          
          if len(items) > 0:
            print('\nWhich item doth thee wish to use, sire?')
            for i in items:
              print('{0} {1}, sire.'.format(i, items[i]))
            print('{0} Return, sire.'.format(len(items)+1))
            
            is_true = False
            while is_true != True:
              is_true = False
              while is_true != True:
                choice = str(input())
                for i in items:
                  if str(choice) == str(i):
                    is_true = True
                    break
            
            if choice == int(len(items)+1):
              is_true = True
            else:
              self.character.inventory[items[choice]] -= 1
              self.character.itemUse(**self.items[items[choice]])
              is_true = False
          
          else:
            print('\nYe haveth not any items in ye inventory, sire.')
            is_true = True                                                        # -- Items --
      
      if encounter.hp <= 0:
        print('\nYe hath killeth thy enemy, sire.')
        defeated = True
        self.character.expUp(encounter.exp_gives)
        self.character.checkExp()
        break                                                                   # -- Character turn --
  
  
  def start(self):
    direction = None
    if isfile(join(path[0], 'save.dat')):
      print('Doth thee wisheth toeth returneth, sire? y/n')
      is_true = False
      while is_true != True:
        direction = str.lower(input())
        if direction == 'y':
          is_true = True
          break
        elif direction == 'n':
          is_true = True
          break
      
    if direction == 'y':
      with open(join(path[0], 'save.dat'), 'rb') as f:
        self.character, self.weapons_prices = pickle.load(f)
      print('Ye hath returneth liketh aeth badasseth, sire.')
    else:
      print('Please enter a name, sire:')
      name = str(input())
      print('Ye hath arriveth at theeth neweth life, sire.')
      self.character = Hero(name)
    
    while self.keep_playing == True:
      print('\nIn which direction doth thee put ones feet, sire?')
      for nmb, lcts in sorted(self.locations['lvl1'].items()):
        print('{0} {1}, sire.'.format(nmb, lcts))
      
      is_true = False
      direction = None
      while is_true != True:
        direction = str(input())
        for i in self.locations['lvl1']:
          if direction == i:
            is_true = True
            break
      
      
      if self.locations['lvl1'][direction] == 'work':                           # -- Work --
        self.character.staminaDown(15)
        if self.character.stamina < 1:
          print('\nYe dieth of exhaustion, sire.')
          self.keep_playing = False
          break
        else:
          print('\nYe completeth the work of thee day, sire.')
          self.character.expUp(5)
          self.character.moneyUp(int(25))                                       # -- Work --
      
        
      elif self.locations['lvl1'][direction] == 'grocery store':                # -- Grocery store --
        print('\nYe attempeth to shopeth for someth groceries, sire.')
        if randint(0, 9) > 5:
          # -- BATTLE --
          self.battle(self.enemies['lvl1'])
          if self.character.stamina > 5:
            self.character.staminaSet(5)
          print('\nYe hath exhausted thyself fighting thee enemy, sire.')
          print('stamina down to {0} points, sire.'.format(
            self.character.stamina
          ))
          # -- BATTLE --
        
        else:
          self.character.staminaUp(5)                                           # -- Grocery store --
      
      
      elif self.locations['lvl1'][direction] == 'dungeon':                      # -- Dungeon --
        print('\nYe attempeth to find thyself an enemy to slay, sire.')
        if randint(0, 9) > 5:
          # -- BATTLE --
          self.battle(self.enemies['lvl1'])
          # -- BATTLE --
        
        else:
          print('\nYe cannot findeth an enemy to slay, sire.')
          self.character.staminaDown(5)                                         # -- Dungeon --
      
      
      elif self.locations['lvl1'][direction] == 'shop':                         # -- Shop --
        shops = {'1': 'weapons shop', '2': 'items shop'}
        print('')
        for i in shops:
          print('{0} {1}, sire.'.format(i, shops[i]))
        
        is_true = True
        while is_true != False:
          choice = str(input())
          for i in shops:
            if choice == str(i):
              choice = str(shops[i])
              is_true = False
        
        if choice == 'weapons shop':                                              # -- Weapons shop --
          print('\nYe hath cometh to inspect ye olde weapons shop, sire.')
          print('Ye haveth {0} golden coins, sire.'.format(
            self.character.money
          ))
          print('\nWhat wisheth ye to buy, sire?')
          loops = 1
          choices = {}
          for i in self.weapons_prices:
            print('{0} {1} {2}, sire.'.format(loops, self.weapons_prices[i], i))
            
            choices[str(loops)] = {}
            choices[str(loops)][i] = self.weapons_prices[i]
            
            loops += 1
          
          is_true = False
          while is_true == False:
            is_true = True
            while is_true != False:
              choice = str(input())
              for i in choices:
                if choice == str(i):
                  is_true = False
            
            for i in choices[choice]:
              product = i
              price = choices[choice][i]
            
            if self.character.money >= int(price):
              self.character.weapon = str(i)
              self.weapons_prices[product] = 0
              self.character.moneyDown(int(self.weapons_prices[i]))
              print('ye haveth bought the {0}, sire.'.format(i))
              is_true = True
              break
            else:
              print('Ye haveth not enough golden coins, sire.')
              is_true = True
              break                                                               # -- Weapons shop --
        
        if choice == 'items shop':                                                # -- Items shop --
          print('\nYe hath cometh to inspect ye olde item shop, sire.')
          print('Ye haveth {0} golden coins, sire.'.format(
            self.character.money
          ))
          print('\nWhat wisheth ye to buy, sire?')
          loops = 1
          choices = {}
          for i in self.items_prices:
            print('{0} {1} {2}, sire.'.format(loops, self.items_prices[i], i))
            
            choices[str(loops)] = {}
            choices[str(loops)][i] = self.items_prices[i]
            
            loops += 1
          
          is_true = False
          while is_true == False:
            is_true = True
            while is_true != False:
              choice = str(input())
              for i in choices:
                if choice == str(i):
                  is_true = False
            
            for i in choices[choice]:
              product = i
              price = choices[choice][i]
            
            print('How many {0}s doth ye wisheth to purchase, sire?'.format(
              product
            ))
            nm = None
            while nm == None:
              choice = str(input())
              nm = re.search('^[0-9]+$', choice)
              if nm != None:
                amnt = nm.group(0)
                if len(amnt) < 1:
                  nm = None
            
            if self.character.money >= int(price) * int(amnt):
              if product in self.character.inventory:
                self.character.inventory[product] += int(amnt)
              else:
                self.character.inventory[product] = int(amnt)
              self.character.moneyDown(int(price) * int(amnt))
              print('ye haveth bought {0} of the {1}, sire.'.format(amnt, i))
              is_true = True
              break
            else:
              print('Ye haveth not enough golden coins, sire.')
              is_true = True
              break                                                               # -- Items shop --
                                                                                # -- Shop --
      
      
      elif self.locations['lvl1'][direction] == 'bed':                          # -- Bed --
        print('\nYe hath goneth to bed, sire')
        self.character.hpSet(self.character.default_hp)
        self.character.staminaSet(self.character.default_stamina)               # -- Bed --
      
      
      elif self.locations['lvl1'][direction] == 'vacation':                     # -- Vacation --
        stay = True
        while stay == True and self.keep_playing == True:
          print('')
          for nmb, lcts in sorted(self.locations['lvl2'].items()):
            print('{0} {1}, sire.'.format(nmb, lcts))
          
          is_true = False
          direction = None
          while is_true != True:
            direction = str(input())
            for i in self.locations['lvl2']:
              if direction == i:
                is_true = True
                break
          
          
          if self.locations['lvl2'][direction] == 'home':                         # -- Home --
            stay = False
            break                                                                 # -- Home --
          
          
          elif self.locations['lvl2'][direction] == 'huge mountain trek':         # -- Huge mountain trek --
            self.battle(self.enemies['lvl3'])                                     # -- Huge mountain trek --
                                                                                # -- Vacation --
      
      elif self.locations['lvl1'][direction] == 'view character':               # -- View character --
        print('\nMax health: {0}, sire.'.format(self.character.default_hp))
        print('Current health: {0}, sire.'.format(self.character.hp))
        print('\nMax stamina: {0}, sire.'.format(
          self.character.default_stamina
        ))
        print('Current stamina: {0}, sire.'.format(self.character.stamina))
        
        print('\nCurrent level: {0}, sire.'.format(self.character.level))
        print('Experience points: {0}, sire.'.format(self.character.experience))
        print('Next level in: {0} points, sire.'.format(
          self.character.exp_limit - self.character.experience
        ))
        
        print('\nWeapon: {0}, sire.'.format(self.character.weapon))
        print('Money: {0}, sire.'.format(self.character.money))
        
        print('\nInventory, sire:')
        for i in self.character.inventory:
          if self.character.inventory[i] > 0:
            print('{0}, amount: {1}, sire.'.format(
              i, self.character.inventory[i]
            ))                                                                  # -- View character --
      
      
      elif self.locations['lvl1'][direction] == 'suicide':                      # -- Suicide --
        print('Doth thee wiseth to record thy adventures in history, sire? y/n')
        is_true = False
        direction = None
        while is_true != True:
          direction = str.lower(input())
          if direction == 'y':
            is_true = True
            break
          elif direction == 'n':
            is_true = True
            break
        
        if direction == 'y':
          with open(join(path[0], 'save.dat'), 'wb') as f:
            pickle.dump(
              [
                self.character, 
                self.weapons_prices
              ], f, protocol=2
            )
        print('\nYe hath given up on yee olde life, sire.')
        self.keep_playing = False
        break                                                                   # -- Suicide --
      
      self.character.checkExp()


def main():
  signal(SIGINT, handler)
  main_game = Game()
  main_game.start()


if __name__ == '__main__':
  main()

