class Hero:
  def __init__(self, name):
    self.name = name
    self.hp = 100
    self.stamina = 30
    self.default_hp = 100
    self.default_stamina = 30

    self.experience = 0
    self.level = 1
    self.exp_limit = 100
    
    self.weapon = 'fists'
    self.money = 0
    
    self.inventory = {}
  
  def levelUp(self):                                                            # -- Level --
    self.level += 1
    self.experience = self.experience - self.exp_limit
    self.exp_limit += self.exp_limit * 0.25
    self.default_hp += self.default_hp * 0.1
    self.default_stamina += self.default_stamina * 0.1
  
  def expUp(self, amount):
    self.experience += amount
    print(str(
      '\nYe hath {0} experience points, '+ 
      'ye needeth another {1} points, sire.'
    ).format(
      self.experience, 
      self.exp_limit - self.experience
    ))
  
  def checkExp(self):
    if self.experience >= self.exp_limit:
      if self.level >= 4:
        print('Ye hath maxeth yee olde level, sire.')
        self.experience = 0
      else:
        self.levelUp()
        
        print('\nYe hath leveleth upeth, sire.')
        print('current level: {0}, sire.'.format(self.level))
        print('HP increased to {0}, sire.'.format(self.default_hp))
        print('Stamina increased to {0}, sire.'.format(self.default_stamina))   # -- Level --
  
  def staminaUp(self, amount):                                                  # -- Stamina --
    self.stamina += amount
    print('stamina up to {0} points, sire.'.format(self.stamina))
  
  def staminaDown(self, amount):
    self.stamina -= amount
    print('stamina down to {0} points, sire.'.format(self.stamina))
  
  def staminaSet(self, amount):
    self.stamina = amount
    print('Stamina at {0}, sire.'.format(self.stamina))                         # -- Stamina --

  def hpSet(self, amount):                                                      # -- HP --
    self.hp = amount
    if self.hp > self.default_hp:
      self.hp = self.default_hp
    print('HP at {0}, sire.'.format(self.hp))                                   # -- HP --
  
  def moneyDown(self, amount):                                                  # -- Money --
    self.money -= amount
    print('Ye haveth {0} coins, sire.'.format(self.money))
  
  def moneyUp(self, amount):
    self.money += amount
    print('Ye haveth {0} coins, sire.'.format(self.money))                      # -- Money --
  
  def itemUse(self, **kwargs):                                                  # -- Items --
    if 'hp' in kwargs:
      self.hpSet(self.hp + kwargs['hp'])
    
    print('{0}'.format(kwargs['message']))                                      # -- Items --


class Enemy:
  def __init__(self, **kwargs):
    self.name = kwargs['name']
    self.hp = kwargs['hp']
    self.level = kwargs['lvl']
    
    self.exp_gives = kwargs['expgive']
    
    self.weapon = kwargs['weapon']
  
  def hpDown(self, amount):
    self.hp -= amount
