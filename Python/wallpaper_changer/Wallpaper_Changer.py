#!/usr/bin/env python3
# -- coding: utf-8 --
'''
Python 3
Sets random new wallpapers.
'''
from sys import exit
from tqdm import tqdm
from time import sleep
from os.path import join
from random import randint
from os import getcwd, walk
from gi.repository import Gio
from signal import signal, SIGINT
from argparse import ArgumentParser


# Handles the keyboard interrupt signal
def handler(signum, b):
  if signum == 2:
    exit(0)


# Walks the current working directory for wallpapers
# and adds what it finds to a list.
def scan(argument):
  wallpapers = []
  extensions = ['.jpg', 'jpeg', '.png']
  
  for path, directories, files in walk(getcwd()):
    for i in files:
      File = join(path, i)
      
      if(
        not File.lower()[-4:] in extensions or
        argument and File.lower().find('nsfw') != -1                                      # Remove paths containing nsfw if argument is given.
      ):
        continue
      
      wallpapers.append(File)
  return wallpapers


if __name__ == '__main__':
  parser = ArgumentParser()
  parser.add_argument('-t', '--timer', type = int, default = 300,                         # Set the timer
            help = 'Delay between setting new wallpapers')
  parser.add_argument('-i', '--ignore', action = 'store_true',                            # Whether to ignore nsfw tagged wallpapers
            help = 'Ignore all filenames with nsfw in them')
  args = parser.parse_args()
  
  signal(SIGINT, handler)                                                                 # A handler for the keyboard interrupt signal
  wallpapers = scan(args.ignore)
  
  source = Gio.SettingsSchemaSource.get_default()
  if source.lookup('org.mate.background', True) is None:                                  # Checks if you have the correct schema.
    print('\nCouldn\'t change wallpaper...')
    exit(0)
  gsettings = Gio.Settings.new('org.mate.background')                                     # Gets the schema
  
  print('Found {0} wallpapers\nTo stop, press ctrl+c'.format(len(wallpapers)))
  while True:
    current = wallpapers[randint(0, len(wallpapers[:-1]))]                                # Select a random wallpaper
    gsettings.set_string('picture-filename', current)                                     # Sets the schema key/wallpaper
    
    print ('\n{0}\nwallpaper changed, sleeping...'.format(current))
    for i in tqdm(range(args.timer)):
      sleep(1)
