function hideshow(arrow, menu){
  if (menu.style.height=="auto"){
    menu.style.height="0"
    menu.style.padding="0"
    menu.style.marginTop="0"
    menu.style.marginBottom="0"
    arrow.style.transform = "rotate(-90deg)"
  }
  else{
    menu.style.height="auto"
    menu.style.padding=".5em"
    menu.style.marginTop=".15em"
    menu.style.marginBottom=".15em"
    arrow.style.transform = "rotate(0deg)"
  }
}
