var randomBack = function(){
  var randomNumber = Math.floor(Math.random()*3);
  var images = [
    "backs/Buzz_Aldrin_by_Neil_Armstrong.jpg",
    "backs/Sunlight_over_Earth_as_seen_by_STS-29_crew.jpg",
    "backs/Ed_White_First_American_Spacewalker.jpg"
  ];
  console.log("Wallpaper:", images[randomNumber]);
  document.getElementById("back").style.backgroundImage = "url(" + images[randomNumber] + ")";
};