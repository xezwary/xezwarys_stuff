#!/bin/bash
for i in ./*.png; do
  if [ ! "$i" = "./*.png" ]; then
    mogrify -format jpg -background black -flatten "$i";    # Change to lossy format.
    rm "$i";                                                # Delete .png version
  fi
  done
for i in ./*.jpg; do
  width=$(identify -format "%w" "$i")
  if [ $width -gt 2560 ]; then                            # If the width is greater than 2560, resize to 2560.
    convert "$i" -resize 2560 "$i";
  fi
  done

echo "Done."
