#!/usr/bin/python3
# -- coding: utf-8 --
'''
Python 3
Generate JS code for selecting background images from the /backs folder
'''
from os.path import abspath, dirname, join, isfile, isdir
from os import listdir, mkdir
from sys import argv

class Main:
  def __init__(self):
    self.path0 = abspath(dirname(argv[0]))
    self.filename = join(abspath(dirname(argv[0])), join('JS', 'randomback.js'))
    self.directory = join(join(abspath(dirname(argv[0])), 'backs'), '')
    self.JavaScript = [
      '''var randomBack = function(){{\n  var randomNumber = Math.floor(\
Math.random()*{0});\n  var images = [''',
      '\n    \"{0}\",',
      '''\n    \"{0}\"\n  ];\n  console.log(\"Wallpaper:\", images[randomNumber]);
  document.getElementById(\"back\").style.backgroundImage = \
\"url(\" + images[randomNumber] + \")\";\n}};'''
    ]
    self.filelist = []
  
  def fileWrite(self, content):
    with open(self.filename, 'a') as i:
      i.write(str(content))
  
  def scan(self, directory):
    for i in listdir(directory):
      fullpath = join(directory, i)
      
      if isfile(fullpath):
        self.filelist.append(fullpath)
      elif isdir(fullpath):
        self.scan(fullpath)
  
  
  def start(self):
    if not isdir(join(self.path0, 'JS')):
      print('Creating '+str(self.filename))
      mkdir(join(self.path0, 'JS'))
    
    elif isfile(self.filename):
      with open(self.filename, 'w') as i:
        i.truncate()
    
    self.scan(self.directory)
    
    self.fileWrite(str(
      self.JavaScript[0].format(len(self.filelist))
    ))
    
    for i in range(0, len(self.filelist)-1):
      self.fileWrite(str(
        self.JavaScript[1].format(self.filelist[i][len(self.path0)+1:])
      ))
    
    self.fileWrite(str(
      self.JavaScript[2].format(self.filelist[-1][len(self.path0)+1:])
    ))

if __name__ == '__main__':
  m = Main()
  m.start()
  print('Done.')
